(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("ELPA"  . "http://tromey.com/elpa/")
			 ("gnu"   . "http://elpa.gnu.org/packages/")
			 ("melpa" . "http://melpa.org/packages/")
			 ;;("org"   . "https://orgmode.org/elpa/")
			 ))
(package-initialize)

;;; *Package* use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Variable" :foundry "PfEd" :slant normal :weight bold :height 110 :width normal)))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "yellow" "#729fcf" "#e090d7" "#8cc4ff" "#9ef"])
 '(custom-safe-themes
   '("0019e9de3b02d21cdf7e5d12bf0c31218772538d0b6eff98c02b0b0d5df8ef94" default))
 '(display-battery-mode t)
 '(package-selected-packages
   '(web-mode use-package try swiper php-mode org-password-manager org-bullets markdown-mode macrostep linum-relative ht haskell-mode go-mode exwm bongo async)))

;;; Include the rest of my configuration.
(when (file-exists-p "~/.emacs.d/config.org")
  (org-babel-load-file "~/.emacs.d/config.org"))
